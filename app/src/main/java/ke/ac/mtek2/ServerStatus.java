package ke.ac.mtek2;

    public class ServerStatus{
        private String Group;
        private String Artifact;
        private String Version;
        private String Status;

        public String getArtifact() {
            return Artifact;
        }

        public String getGroup() {
            return Group;
        }

        public String getStatus() {
            return Status;
        }

        public String getVersion() {
            return Version;
        }

        public void setArtifact(String artifact) {
            Artifact = artifact;
        }

        public void setGroup(String group) {
            Group = group;
        }

        public void setStatus(String status) {
            Status = status;
        }

        public void setVersion(String version) {
            Version = version;
        }



}
