package ke.ac.mtek2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import ke.ac.mtek2.ui.TwitterActivity;


public class MainActivity extends AppCompatActivity {
    private Button button;
    private Button button3;
    private Button button4;
    private Button button5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       this.button =findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMain2Activity();
            }
        });
        this.button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFacebookActivity();
            }
        });
        this.button4 = findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View p) {
                openTwitterActivity();
            }
        });





    }

public void openMain2Activity() {
    Intent intent = new Intent(this, Main2Activity.class);
    startActivity(intent);
    Log.e("MA","The user clicked login button");
            Log.i("MA","OnClick()<-");

}



public void openFacebookActivity(){
        Intent i = new Intent(this, FacebookActivity.class);
                startActivity(i);
}
public void  openTwitterActivity(){
        Intent i = new Intent(this, TwitterActivity.class);
        startActivity(i);
}
public void openGmail(){
        Intent i = new Intent(this,Gmail.class);
        startActivity(i);
}
}
